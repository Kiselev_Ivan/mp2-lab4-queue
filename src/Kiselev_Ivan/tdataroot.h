// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"
#include <quest.h>

#define DefMemSize   20  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef Quest  TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef Quest    TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom
{
protected:
  PTElem pMem;      // память для СД
  int MemSize;      // размер памяти для СД
  int DataCount;    // количество элементов в СД
  TMemType MemType; // режим управления памятью
public:
  virtual ~TDataRoot();
  TDataRoot(int Size = DefMemSize);
  virtual bool IsEmpty(void) const;           // контроль пустоты СД
  virtual bool IsFull (void) const;           // контроль переполнения СД
  virtual void  Put   (const PTElem);		 // добавить значение
  virtual TData Get   (void);				 // извлечь значение

};

TDataRoot::TDataRoot(int Size)
{
	if (Size < 1) Size = 1;	
	MemSize = Size;
	pMem = new TElem(Size);
	DataCount = 0;
	MemType = MEM_HOLDER;
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;
}

TData TDataRoot::Get(void)
{
	TData res(10);
	return res;
}

void TDataRoot::Put(PTElem elem) {};

bool TDataRoot::IsFull(void) const {
	return DataCount == MemSize;
}

bool TDataRoot::IsEmpty(void) const {
	return DataCount == 0;
}

#endif
