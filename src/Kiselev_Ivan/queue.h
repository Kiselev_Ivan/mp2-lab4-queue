#ifndef _Queue_
#define _Queue_

#include "tdataroot.h"


using namespace std;
class Queue : public TDataRoot
{
	private:
		int start,end;				//������ � ����� �������
	public:
		Queue(int Size = DefMemSize);				//�����������
		bool IsEmpty(void) const;					//�����?
		bool IsFull(void) const;					//������?
		void Put(PTElem quest,int* miss);			//��������
		TData Get();								//�����
		int GetHMElements();
};

int Queue::GetHMElements()
{
	return DataCount;
}

Queue::Queue(int Size)
{
	if (Size < 1) Size = 1;
	pMem = new TElem[Size];
	MemSize = Size;
	DataCount = 0;
	start = 0;
	end = 0;
};

bool Queue::IsEmpty(void) const 
{
	return DataCount == 0;
};

bool Queue::IsFull(void) const 
{
	return DataCount == MemSize;
};


void Queue::Put (PTElem quest,int* miss)	
{
	if (!IsFull()) 
	{
		pMem[end] = *(quest);
		//cout << "Quest received" << endl;
		DataCount ++;
		end ++;
		if (end > MemSize-1)
			end = 0;			//����� �� ������� �������
	}
	else
	{
		*(miss) += 1;			//������� �����������
		//cout << "MISS" << endl;
	}
	
};

TData Queue::Get()
{
	if (IsEmpty()) { throw 1; };
	TData res = pMem[start];
	start += 1;
	if (start > MemSize-1) start = 0;	//����� �� ������� �������
	DataCount -= 1;
	return res;

};


#endif